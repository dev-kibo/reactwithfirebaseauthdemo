import React, { useContext } from "react";
import { Container } from "semantic-ui-react";
import "./App.css";
import { Switch, Route } from "react-router-dom";
// components
import Navigation from "./components/Navigation";
import Login from "./components/Login";
import Register from "./components/Register";
import ForgotPassword from "./components/ForgotPassword";
import Home from "./components/Home";
import PrivateRoute from "./components/PrivateRoute";

import { AuthContext } from "./Auth";

export default function App() {
  const { currentUser } = useContext(AuthContext);

  return (
    <Container className="App">
      {currentUser !== null ? <p>Email: {currentUser.email}</p> : ""}
      <Navigation />
      <Switch>
        <PrivateRoute exact path="/" component={Home} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/signup" component={Register} />
        <Route exact path="/resetpassword" component={ForgotPassword} />
      </Switch>
    </Container>
  );
}
