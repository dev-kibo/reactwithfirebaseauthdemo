import React, { useState, useEffect } from "react";
import { Image, List, Card, Grid, Flag, Pagination } from "semantic-ui-react";
import axios from "axios";

export default function Home() {
  const [activePage, setActivePage] = useState(1);
  const [dataPerPage, setDataPerPage] = useState([]);
  const [data, setData] = useState([]);

  useEffect(() => {
    const result = axios("https://randomuser.me/api/?results=100&seed=abc");
    result.then(users => setData(users.data.results));
  }, []);

  useEffect(() => {
    const indexOfLastUsers = activePage * 10;
    const indexOfFirstUsers = indexOfLastUsers - 10;
    const currentUsers = data.slice(indexOfFirstUsers, indexOfLastUsers);

    setDataPerPage(currentUsers);
  }, [activePage, data]);

  const handlePageChange = (e, { activePage }) => {
    setActivePage(activePage);
  };

  return (
    <Grid divided="vertically">
      <Grid.Row stretched columns={5}>
        {dataPerPage.map(user => {
          return (
            <Grid.Column key={user.login.uuid}>
              <Card>
                <Image src={user.picture.large} wrapped ui={false} />
                <Card.Content>
                  <Card.Header>
                    <span>
                      <Flag name={user.nat.toLowerCase()} />
                    </span>
                    {user.name.first} {user.name.last}
                  </Card.Header>
                  <Card.Meta>{user.id.value}</Card.Meta>
                  <Card.Description>
                    <p>
                      Lives in {user.location.city}, {user.location.state}
                    </p>
                    <List>
                      <List.Item>
                        Address: {user.location.street.name}{" "}
                        {user.location.street.number}
                      </List.Item>
                    </List>
                  </Card.Description>
                </Card.Content>
                <Card.Content>
                  <p>
                    {user.dob.age}{" "}
                    {user.gender.charAt(0).toUpperCase() + user.gender.slice(1)}
                  </p>
                  <p>Contact: {user.phone}</p>
                </Card.Content>
              </Card>
            </Grid.Column>
          );
        })}
      </Grid.Row>
      <Grid.Row textAlign="center">
        <Grid.Column>
          <Pagination
            activePage={activePage}
            onPageChange={handlePageChange}
            totalPages={10}
          />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
}
