import React from "react";
import { Grid } from "semantic-ui-react";
import RegisterForm from "./Form/RegisterForm";

export default function Register() {
  return (
    <Grid verticalAlign="middle" centered padded>
      <Grid.Row>
        <Grid.Column width={6}>
          <RegisterForm />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
}
