import React from "react";
import { Grid } from "semantic-ui-react";
import LoginForm from "./Form/LoginForm";

export default function Login() {
  return (
    <Grid verticalAlign="middle" centered padded>
      <Grid.Row>
        <Grid.Column width={6}>
          <LoginForm />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
}
