import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import { Menu, Button } from "semantic-ui-react";
import app from "../Base";
import { AuthContext } from "../Auth";

export default function Navigation() {
  const { currentUser } = useContext(AuthContext);
  return (
    <>
      <Menu>
        <Menu.Item header>Meals</Menu.Item>
        <Menu.Item name="home">
          <NavLink to="/">Home</NavLink>
        </Menu.Item>
        <Menu.Menu position="right">
          {currentUser ? (
            <Menu.Item name="logout">
              <Button onClick={() => app.auth().signOut()}>Sign Out</Button>
            </Menu.Item>
          ) : (
            <>
              <Menu.Item name="login">
                <NavLink to="/login">Login</NavLink>
              </Menu.Item>
              <Menu.Item name="signup">
                <NavLink to="/signup">Sign Up</NavLink>
              </Menu.Item>
            </>
          )}
        </Menu.Menu>
      </Menu>
    </>
  );
}
