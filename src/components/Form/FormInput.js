import React from "react";
import { Form } from "semantic-ui-react";

export default function FormInput({
  name,
  label,
  placeholder,
  onChange,
  type
}) {
  return (
    <Form.Input
      name={name}
      label={label}
      placeholder={placeholder}
      onChange={onChange}
      type={type}
    ></Form.Input>
  );
}
