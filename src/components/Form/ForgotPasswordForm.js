import React, { useState } from "react";
import { Form, Button, Segment, Grid, Message } from "semantic-ui-react";
import FormInput from "./FormInput";

export default function LoginForm() {
  const [email, setEmail] = useState("");
  const [errorMsgs, setErrorMsgs] = useState([]);

  const onChangeEmail = e => {
    setEmail(e.target.value);
  };

  const validateForm = e => {
    e.preventDefault();
    const errors = [];
    if (email === "") {
      errors.push("You must enter your email");
    } else if (
      !/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        email
      )
    ) {
      errors.push("Email contains invalid characters");
    }
    if (errors.length > 0) {
      setErrorMsgs(errors);
    }
  };

  return (
    <Segment raised>
      {errorMsgs.length > 0 ? <Message error list={errorMsgs} /> : ""}
      <Form onSubmit={validateForm}>
        <FormInput
          name="email"
          label="Email"
          placeholder="Email"
          onChange={onChangeEmail}
          type="email"
        />
        <Grid columns="equal" verticalAlign="middle" padded>
          <Grid.Row>
            <Grid.Column textAlign="left">
              <a href="/login" rel="noopener noreferrer">
                Login
              </a>
            </Grid.Column>
            <Grid.Column textAlign="right">
              <Button type="submit" color="blue">
                Send link
              </Button>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Form>
    </Segment>
  );
}
