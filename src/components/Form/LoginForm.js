import React, { useState, useContext } from "react";
import { Form, Button, Segment, Grid, Message } from "semantic-ui-react";
import { withRouter, Redirect } from "react-router-dom";
import app from "../../Base";
import { AuthContext } from "../../Auth";
import FormInput from "./FormInput";

function LoginForm({ history }) {
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const [isPassWrong, setIsPassWrong] = useState(false);
  const [errorMsgs, setErrorMsgs] = useState([]);

  const onChangeEmail = e => {
    setEmail(e.target.value);
  };

  const onChangePass = e => {
    setPass(e.target.value);
  };

  const validateForm = e => {
    e.preventDefault();
    const errors = [];
    if (email === "" && pass === "") {
      errors.push("You must fill all the fields");
    } else if (pass !== "sifra123") {
      errors.push("Username or Password is wrong");
      setIsPassWrong(true);
    }
    if (errors.length > 0) {
      setErrorMsgs(errors);
    } else {
      app
        .auth()
        .signInWithEmailAndPassword(email, pass)
        .then(res => history.push("/"))
        .catch(error => setErrorMsgs([error.message]));
    }
  };

  const { currentUser } = useContext(AuthContext);

  if (currentUser) {
    return <Redirect to="/" />;
  }

  return (
    <Segment raised>
      {errorMsgs.length > 0 ? <Message error list={errorMsgs} /> : ""}
      <Form onSubmit={validateForm}>
        <FormInput
          name="email"
          label="Email"
          placeholder="Email"
          onChange={onChangeEmail}
          type="email"
        />
        <FormInput
          name="password"
          label="Password"
          placeholder="Password"
          type="password"
          onChange={onChangePass}
        />
        <Grid columns="equal" verticalAlign="middle" padded>
          <Grid.Row>
            <Grid.Column textAlign="left">
              <a href="/signup" rel="noopener noreferrer">
                Sign Up
              </a>
            </Grid.Column>
            <Grid.Column textAlign="right">
              <Button type="submit" color="teal">
                Authenticate
              </Button>
            </Grid.Column>
          </Grid.Row>
          {isPassWrong ? (
            <Grid.Row textAlign="center">
              <Grid.Column>
                <a href="/resetpassword">Forgot password? Click Here</a>
              </Grid.Column>
            </Grid.Row>
          ) : (
            ""
          )}
        </Grid>
      </Form>
    </Segment>
  );
}

export default withRouter(LoginForm);
