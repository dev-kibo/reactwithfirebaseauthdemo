import React, { useState } from "react";
import { Form, Button, Segment, Grid, Message } from "semantic-ui-react";
import FormInput from "./FormInput";
import { withRouter } from "react-router-dom";
import app from "../../Base";

function RegisterForm({ history }) {
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const [rpass, setRPass] = useState("");
  const [errorMsgs, setErrorMsgs] = useState([]);

  const onChangeEmail = e => {
    setEmail(e.target.value);
  };

  const onChangePass = e => {
    setPass(e.target.value);
  };

  const onChangeRPass = e => {
    setRPass(e.target.value);
  };

  const validateForm = e => {
    e.preventDefault();
    const errors = [];
    if (email === "" && pass === "" && rpass === "") {
      errors.push("You must fill all the fields");
    } else if (
      !/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        email
      )
    ) {
      errors.push("Email contains invalid characters");
    } else if (pass.length < 8) {
      errors.push("Password must be at least 8 characters");
    } else if (pass !== rpass) {
      errors.push("Passwords do not match");
    }
    if (errors.length > 0) {
      setErrorMsgs(errors);
    } else {
      app
        .auth()
        .createUserWithEmailAndPassword(email, pass)
        .then(res => app.auth().signOut())
        .then(res => history.push("/login"))
        .catch(error => setErrorMsgs([error.message]));
    }
  };

  return (
    <Segment raised>
      {errorMsgs.length > 0 ? <Message error list={errorMsgs} /> : ""}
      <Form onSubmit={validateForm}>
        <FormInput
          name="email"
          label="Email"
          placeholder="Email"
          onChange={onChangeEmail}
          type="email"
        />
        <FormInput
          name="password"
          label="Password"
          placeholder="Password"
          type="password"
          onChange={onChangePass}
        />
        <FormInput
          name="c_password"
          label="Confirm Password"
          placeholder="Repeat Password"
          type="password"
          onChange={onChangeRPass}
        />
        <Grid columns="equal" verticalAlign="middle" padded>
          <Grid.Row>
            <Grid.Column textAlign="left">
              <a href="/login" rel="noopener noreferrer">
                Login
              </a>
            </Grid.Column>
            <Grid.Column textAlign="right">
              <Button type="submit" color="teal">
                Register
              </Button>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Form>
    </Segment>
  );
}

export default withRouter(RegisterForm);
