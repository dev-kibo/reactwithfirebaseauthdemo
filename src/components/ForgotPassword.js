import React from "react";
import { Grid } from "semantic-ui-react";
import ForgotPasswordForm from "./Form/ForgotPasswordForm";

export default function Login() {
  return (
    <Grid verticalAlign="middle" centered padded>
      <Grid.Row>
        <Grid.Column width={6}>
          <ForgotPasswordForm />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
}
