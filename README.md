## Table of contents
* [Demo](#demo)
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

# [Click here to view live demo](https://react-firebase-auth-demo.herokuapp.com)

## General info
Demonstrates use of Firebase Authentication in React application.    
User creates an account in order to see some data.    
Data fetched from [Random User](https://randomuser.me)    


## Technologies
Project was created with:
* [React](https://reactjs.org/)
* [Google Firebase](https://firebase.google.com/)
* [Semantic UI](https://semantic-ui.com/)
	
## Setup
To run this project, install it locally using npm:

```
$ npm i
$ npm start
```